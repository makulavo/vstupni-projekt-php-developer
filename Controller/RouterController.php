<?php

class RouterController extends Controller
{

    protected Controller $controller;

    function process(array $parameters): void
    {
        $parsedUrl = $this->parseUrl($parameters[0]);

        if(empty($parsedUrl[0])){
            $this->reroute("zakaznik/registrace");
        }

        $controllerClassName = $this->convertToClassName(array_shift($parsedUrl)) . "Controller";

        if(file_exists("Controller/".$controllerClassName.".php")){
            $this->controller = new $controllerClassName;
            $this->controller->process($parsedUrl);
        } else {
            $this->reroute("error");
        }
        $this->data["title"] = $this->controller->head["title"];
        $this->data["scripts"] = $this->controller->head["scripts"];
        $this->data["flashes"] = $this->controller->flashes;
        $this->view = "layout";
    }


    private function parseUrl(string $url) : array
    {
        $parsedUrl = parse_url($url);
        $parsedUrl["path"] = ltrim($parsedUrl["path"], "/");
        $parsedUrl["path"] = trim($parsedUrl["path"]);
        return explode("/", $parsedUrl["path"]);
    }

    private function convertToClassName(string $urlName) : string
    {
        return str_replace(" ", "", ucwords(str_replace("-", " ", $urlName)));
    }
}