<?php

class ItemController extends Controller
{
    private ItemModel $model;

    function process(array $parameters) : void
    {
        $this->model = new ItemModel();

        $this->head["title"] = "Přidat zboží";
        $this->data["errors"] = [];

        if(!empty($_POST["form_item_add"])){
            $result = $this->model->insertItem($_POST);
            if($result === true) {
                $this->flashes[] = "Insertion successfull";
            }else{
                $this->data["errors"] = $result;
            }
        }

        $this->view = "item.add";
    }
}