<?php

class GeneratorController extends Controller
{
    private GeneratorModel $model;

    function process(array $parameters): void
    {
        $this->model = new GeneratorModel();

            $this->head["title"] = "Generování karet";
            $this->data["generated_cards"] = [];

            if(isset($_POST["card_form"]))
            {
                if(!empty($_POST["card_count"]) && $_POST["card_count"] > 0){
                    $this->data["generated_cards"] = $this->model->generateCards($_POST["card_count"]);
                }else {
                    $this->data["error"] = "Invalid card count";
                }
            }
            if(isset($_POST["purchase_form"]))
            {
                if(!empty($_POST["purchase_count"]) && $_POST["purchase_count"] > 0){
                    $this->data["generated_purchases"] = $this->model->generatePurchases($_POST["purchase_count"]);
                }else {
                    $this->data["error"] = "Invalid purchase count";
                }
            }

            $this->view = "generator";

    }
}