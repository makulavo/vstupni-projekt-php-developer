<?php

class CustomerController extends Controller
{
    private CustomerModel $model;
    private CardModel $cardModel;

    public function process(array $parameters): void
    {
        $this->model = new CustomerModel();
        $this->cardModel = new CardModel();

        if($parameters[0] == "register" || $parameters[0] == null) {
            $this->head["title"] = "Registrace zákazníka";
            $this->data["errors"] = [];

            $this->data["cards"] = $this->cardModel->getAllCards();

            if(isset($_POST["form_register"])) {
                $errors = $this->model->validateCustomerForm($_POST);
                if($errors === true){
                    if ($this->model->insertNewCustomer($_POST) > 0) {
                        $this->flashes[] = "Insert successfull";
                    }
                }else {
                    $this->data["errors"] = $errors;
                }
            }
            $this->view = "customer.register";
        }else if($parameters[0] === "report"){
            $this->head["title"] = "Report";

            $this->data["customers"] = $this->model->getAllCustomers();
            $this->data["cards"] = $this->cardModel->getUsedCards();
            $this->data["topTenCustomers"] = $this->model->getTopTenCustomers();

            $this->view = "customer.report";
        }
    }
}