<?php


abstract class Controller
{
    protected array $data = [];
    protected string $view = "";
    protected  array $head = ["title"=>"","scripts"=>[]];
    protected array $flashes = [];

    abstract function process(array $parameters) : void;

    public function drawView() : void
    {
        if($this->view)
        {
            extract($this->data);
            require "View/$this->view.phtml";
        }
    }

    public function reroute(string $url) : void
    {
        header("Location: /$url");
        header("Connection: close");
    }
}