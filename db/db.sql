SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

DROP SCHEMA IF EXISTS `vstupniprojektdb` ;

CREATE SCHEMA IF NOT EXISTS `vstupniprojektdb` DEFAULT CHARACTER SET utf8 ;
USE `vstupniprojektdb` ;

DROP TABLE IF EXISTS `vstupniprojektdb`.`customer` ;

CREATE TABLE IF NOT EXISTS `vstupniprojektdb`.`customer` (
                                                 `id` INT NOT NULL AUTO_INCREMENT,
                                                 `name` VARCHAR(45) NOT NULL,
    `surname` VARCHAR(45) NOT NULL,
    `email` VARCHAR(45) NOT NULL,
    `phone` VARCHAR(45) NULL,
    `registered` DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `vstupniprojektdb`.`address` ;

CREATE TABLE IF NOT EXISTS `vstupniprojektdb`.`address` (
                                                `id` INT NOT NULL AUTO_INCREMENT,
                                                `street` VARCHAR(45) NOT NULL,
    `city` VARCHAR(45) NOT NULL,
    `zip` INT NOT NULL,
    `country` VARCHAR(45) NOT NULL,
    `address_type` ENUM('shipping', 'billing') NOT NULL,
    `address_name` VARCHAR(45) NOT NULL,
    `customer_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `idtable1_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_address_customer_idx` (`customer_id` ASC) VISIBLE,
    CONSTRAINT `fk_address_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `vstupniprojektdb`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `vstupniprojektdb`.`card` ;

CREATE TABLE IF NOT EXISTS `vstupniprojektdb`.`card` (
                                             `id` INT NOT NULL AUTO_INCREMENT,
                                             `number` INT NOT NULL,
                                             `type` ENUM('plastic', 'virtual') NOT NULL,
    `customer_id` INT,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `idcard_UNIQUE` (`id` ASC) VISIBLE,
    UNIQUE INDEX `number_UNIQUE` (`number` ASC) VISIBLE,
    INDEX `fk_card_customer1_idx` (`customer_id` ASC) VISIBLE,
    CONSTRAINT `fk_card_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `vstupniprojektdb`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `vstupniprojektdb`.`item` ;

CREATE TABLE IF NOT EXISTS `vstupniprojektdb`.`item` (
                                             `id` INT NOT NULL AUTO_INCREMENT,
                                             `name` VARCHAR(45) NOT NULL,
    `price` DECIMAL(6,2) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `vstupniprojektdb`.`receipt` ;

CREATE TABLE IF NOT EXISTS `vstupniprojektdb`.`receipt` (
                                                `id` INT NOT NULL AUTO_INCREMENT,
                                                `date` DATETIME NOT NULL DEFAULT NOW(),
                                                `card_id` INT NOT NULL,
                                                PRIMARY KEY (`id`, `card_id`),
    UNIQUE INDEX `idreceipt_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_receipt_card1_idx` (`card_id` ASC) VISIBLE,
    CONSTRAINT `fk_receipt_card1`
    FOREIGN KEY (`card_id`)
    REFERENCES `vstupniprojektdb`.`card` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `vstupniprojektdb`.`receipt_item` ;

CREATE TABLE IF NOT EXISTS `vstupniprojektdb`.`receipt_item` (
                                                     `receipt_id` INT NOT NULL,
                                                     `item_id` INT NOT NULL,
                                                     `quantity` INT NOT NULL,
                                                     PRIMARY KEY (`receipt_id`, `item_id`),
    INDEX `fk_receipt_has_item_item1_idx` (`item_id` ASC) VISIBLE,
    INDEX `fk_receipt_has_item_receipt1_idx` (`receipt_id` ASC) VISIBLE,
    CONSTRAINT `fk_receipt_has_item_receipt1`
    FOREIGN KEY (`receipt_id`)
    REFERENCES `vstupniprojektdb`.`receipt` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_receipt_has_item_item1`
    FOREIGN KEY (`item_id`)
    REFERENCES `vstupniprojektdb`.`item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
