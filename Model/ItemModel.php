<?php

class ItemModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insertItem(array $postData) : bool|array {
        $errors = $this->validateItemData($postData);
        if(empty($errors)){
            $query = "INSERT INTO item(name, price) VALUES (?, ?);";
            $params = [
              $postData["name"],
              $postData["price"]
            ];
            if($this->query($query, $params) > 0){
                return true;
            } else {
                $errors[] = "Something went wrong";
            }
        }
        return $errors;
    }

    public function getAllItems() : array
    {
        $query = "SELECT * FROM item;";
        return $this->fetchAll($query);
    }

    private function validateItemData(array $postData) : array
    {
        $errors = [];
        if(empty($postData["name"]) || !preg_match("/^\w+$/",$postData["name"])){
            $errors[] = "Name is incorrect";
        }
        if(empty($postData["price"]) || !is_numeric($postData["price"])){
            $errors[] = "Price is incorrect";
        }
        return $errors;
    }
}