<?php

abstract class Model
{
    //normálně by to mělo být v config filu, kterej by nebyl na gitu. Ale pro jednoduchost to nechám tady.
    private const SERVER = "localhost";
    private const DB_NAME = "vstupniprojektdb";
    private const USERNAME = "root";
    private const PASSWORD = "";
    private  const SETTINGS = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    private PDO $dbConn;

    protected function __construct()
    {
        $this->dbConn = new PDO("mysql:host=".self::SERVER.";dbname=".self::DB_NAME,
            self::USERNAME,
            self::PASSWORD,
             self::SETTINGS);
    }

    protected function fetchSingle(string $query, array $params = []) : array|bool
    {
        $values = $this->dbConn->prepare($query);
        $params = $this->clearParams($params);
        $values->execute($params);
        return $values->fetch(PDO::FETCH_ASSOC);
    }

    protected function fetchAll(string $query, array $params = []) : array|bool
    {
        $values = $this->dbConn->prepare($query);
        $params = $this->clearParams($params);
        $values->execute($params);
        return $values->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function query(string $query, array $params = []) : int
    {
        $values = $this->dbConn->prepare($query);
        $params = $this->clearParams($params);
        $values->execute($params);
        return $values->rowCount();
    }

    protected function insertAndGetId(string $query, array $params = []) : int
    {
        $values = $this->dbConn->prepare($query);
        $params = $this->clearParams($params);
        $values->execute($params);
        return $this->dbConn->lastInsertId();
    }

    private function clearParams(array $params) : array
    {
        foreach ($params as $key => $param) {
            if(is_string($param)) {
                $params[$key] = htmlspecialchars(trim($param));
            }
        }
        return $params;
    }
}