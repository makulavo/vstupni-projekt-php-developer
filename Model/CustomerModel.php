<?php

class CustomerModel extends Model
{
    private const MAIL = "test@mail.cz";

    public function __construct()
    {
        parent::__construct();
    }

    public function validateCustomerForm(array $postData) : array|bool
    {
        $errors = array_merge($this->validateCustomerInfo($postData), $this->validateCustomerAddress($postData));
        return empty($errors) ? true : $errors;
    }

    public function insertNewCustomer(array $postData) : bool
    {
        $query = "INSERT INTO customer(name, surname, email, phone) VALUES (?,?,?,?);";
        $params = [
          $postData["name"],
          $postData["surname"],
          $postData["email"],
            str_replace(" ", "", $postData["phone"]),
        ];

        $id = $this->insertAndGetId($query, $params);
        if($id > 0) {
            $addressQuery = "INSERT INTO address(street, city, zip, country, address_type, address_name, customer_id) VALUES (?,?,?,?,?,?,?);";
            $addressParams = [
                $postData["street"],
                $postData["city"],
                $postData["zip"],
                $postData["country"],
                $postData["address_type"],
                $postData["address_name"],
                $id
            ];

            $cardQuery = "UPDATE card SET customer_id = ? WHERE card.id = (SELECT * FROM (SELECT id FROM card ORDER BY number ASC LIMIT 1) c);";

            if($this->query($addressQuery, $addressParams) > 0 &&
                $this->query($cardQuery, [$id]) > 0){
                $this->sendMail($postData);
                return true;
            }
        }
        return false;
    }

    public function getAllCustomers() : array
    {
        $query = "SELECT * FROM customer;";
        return $this->fetchAll($query);
    }

    public function getTopTenCustomers() : array
    {
        $query = "SELECT customer.*, SUM(item.price * receipt_item.quantity) as suma
                    FROM receipt
                        JOIN receipt_item ON receipt.id = receipt_item.receipt_id 
                        JOIN item ON receipt_item.item_id = item.id 
                        JOIN card ON card.id = receipt.card_id 
                        JOIN customer ON customer.id = card.customer_id 
                    WHERE (receipt.date BETWEEN NOW()- INTERVAL 30 DAY AND NOW()) 
                    GROUP BY customer.id 
                    ORDER BY suma LIMIT 0, 10;";
        return $this->fetchAll($query);
    }

    private function validateCustomerInfo(array $postData) : array
    {
        $errors = [];

        if(empty($postData["name"]) || !preg_match("/^[A-Z][a-z]{2,}$/", $postData["name"])){
            $errors[] = "Name is incorrect";
        }
        if(empty($postData["surname"]) || !preg_match("/^[A-Z][a-z]{2,}$/", $postData["surname"])){
            $errors[] = "Surname is incorrect";
        }
        if(empty($postData["email"]) || !preg_match("/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9_.-]+\.[a-z]{2,3}$/", $postData["email"])){
            $errors[] = "E-mail is incorrect";
        } else if (count($this->fetchAll("SELECT * FROM customer WHERE email LIKE ?", [$postData["email"]])) > 0){
            $errors[] = "This e-mail is already used";
        }
        str_replace(" ", "", $postData["phone"]);
        if(empty($postData["phone"]) || !preg_match("/^\d{9}$/", $postData["phone"])){
            $errors[] = "Phone is incorrect";
        } else if (count($this->fetchAll("SELECT * FROM customer WHERE phone LIKE ?", [$postData["phone"]])) > 0){
            $errors[] = "This phone is already used";
        }

        return $errors;
    }

    private function validateCustomerAddress(array $postData) : array
    {
        $errors = [];

        if(empty($postData["street"])){
            $errors[] = "Street is incorrect";
        }
        if(empty($postData["city"])){
            $errors[] = "City is incorrect";
        }
        if(empty($postData["zip"]) || !preg_match("/^\d{3,5}$/",$postData["zip"])){
            $errors[] = "ZIP code is incorrect";
        }
        if(empty($postData["country"])){
            $errors[] = "Country is incorrect";
        }
        if(empty($postData["address_type"]) || !in_array($postData["address_type"], ["billing", "shipping"])){
            $errors[] = "Address type is incorrect";
        }
        if(empty($postData["address_name"])){
            $errors[] = "Address name is incorrect";
        }

        return $errors;
    }

    private function sendMail($postData) : void
    {
        $to = $postData["mail"];
        $subject = "Registration confirmation";
        $message = "Hi,".$postData["name"]." ".$postData["surname"].", you were just registered at our test app";
        $headers = "From: " .self::MAIL. "\r\n" .
            "Reply-To: " .self::MAIL. "\r\n" .
            "X-Mailer: PHP/" . phpversion();
    }
}