<?php

class GeneratorModel extends Model
{
    private CardModel $cardModel;
    private ItemModel $itemModel;

    public function __construct()
    {
        parent::__construct();
        $this->cardModel = new CardModel();
        $this->itemModel = new ItemModel();
    }

    public function generateCards(int $count) : array
    {
        $generatedCards = [];

        $query = "SELECT MAX(number) as number FROM card;";
        $lastNumber = $this->fetchSingle($query)["number"] ?? 10000;
        for ($i = 0; $i < $count; $i++) {
            $generatedCards[$i] = [
              "number" => ++$lastNumber,
              "type" => rand(0, 1) == 0 ? "plastic" : "virtual"
            ];
        }

        $insertQuery = "INSERT INTO card(number, type) VALUES ";
        $params = [];
        foreach ($generatedCards as $generatedCard) {
            $insertQuery .= "(?,?),";
            $params[] = $generatedCard["number"];
            $params[] = $generatedCard["type"];
        }
        $insertQuery = substr_replace($insertQuery, ";", -1);
        $this->query($insertQuery, $params);

        return $generatedCards;
    }

    public function generatePurchases(int $count) : array
    {
        $cards = $this->cardModel->getUsedCards();
        if(count($cards) <= 0){
            throw new Exception("Generator cannot be used. There aren't any cards in use!");
        }
        $items = $this->itemModel->getAllItems();
        if(count($items) <= 0){
            throw new Exception("Generator cannot be used. There aren't any items to buy!");
        }
        $generatedPurchases = [];
        for ($i = 0; $i < $count; $i++) {
            $generatedPurchases[$i] = [
                "card_id" => $cards[rand(0, count($cards)-1)]["id"],
                "items" => []
            ];
            $receiptQuery = "INSERT INTO receipt(card_id) values (?)";
            $id = $this->insertAndGetId($receiptQuery, [$generatedPurchases[$i]["card_id"]]);

            for ($j = 0; $j < rand(1,count($items)-1); $j++){
                $generatedPurchases[$i]["items"][$j] = [
                    "item_id" => $items[$j]["id"],
                    "quantity" => rand(1, 7)
                ];

                $itemQuery = "INSERT INTO receipt_item(receipt_id, item_id, quantity) VALUES (?, ?, ?);";
                $item = $generatedPurchases[$i]["items"][$j];
                $this->query($itemQuery, [$id, $item["item_id"], $item["quantity"]]);
            }
        }
        return $generatedPurchases;
    }
}