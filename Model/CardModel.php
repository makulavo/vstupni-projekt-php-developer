<?php

class CardModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param bool $withoutUser if true, selects only cards, that aren't asociated with any user
     * @return array associative array of cards
     */
    public function getAllCards(bool $withoutUser = true) : array
    {
        if ($withoutUser) {
            $query = "SELECT * FROM card WHERE customer_id IS NULL;";
        }else {
            $query = "SELECT * FROM card;";
        }
        return $this->fetchAll($query);
    }

    public function getUsedCards() : array
    {
        $query = "SELECT * FROM card WHERE customer_id IS NOT NULL;";
        return $this->fetchAll($query);
    }
}