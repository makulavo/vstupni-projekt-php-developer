
# Vstupní projekt PHP developer

### 0. Setup mvc projektu

*commity začínající #0*
- základní nastavení mvc aplikace (vytvoření index.php, routeru a základního kontroleru ze kterého budou ostatní kontrolery dědit)

### 1. Vytvoření databáze

*commity začínající #1*
- vytvoření er diagramu
  - zákazník může mít více adres, např. jednu fakturační a dvě dodací
  - každá karta může mít jednoho majitele, ale zákazníci mohou mít více karet
  - každá účtenka může být přiřazena k jedné kartě.
  - na každé účtence může být více produktů a každý produkt může být na více účtenkách
- vytvoření sql skriptu pro vytvoření databáze podle er diagramu

### 2. Vytvoření základního modelu a připojení k db

*commity začínající #2*
- Vytvoření modelu, ze kterého budou ostatní modely dědit
  - připojení k databázi přes PDO

### 3. Vytvoření modelu a controlleru pro registraci zákazníka

*commity začínající #3*
- Vytvoření kontroleru pro zákazníky
- Vytvoření modelu pro práci se zákazníky
  - vytvoření validačních metod pro validaci:
    - informací zákazníka
    - adresy
    - propojení validace a výpisu errorů přes kontroler
  - vytvoření metody pro vložení zákazníka

### 4. Funkcionalita karet

*commity začínající #4*
- vytvoření kontroleru pro karty
- vytvoření pohledu pro karty
- vytvoření modelu pro karty
  - metoda pro generování určeného počtu karet
  - metoda pro získání všech karet karet
- přidání pole karty do formuláře pro přidání karty

### 5. Nákupy

*commity začínající #5*
- vytvoření logiky pro přidání itemů obchodu
  - model, view, controller
- vytvoření logiky pro nákupy
  - model, view controller
  - Generátor dat

### 6. Report

*commity začínající #6*
- report view
- úprava controlleru
- přidání metody do modelu pro získání top 10 zákazníků

### 7. B4

*commity začínající #7*
1. realizováno přes flash message
2. validační metody v modelu, vrací zpět pole errorů, které je možno zobrazit uživateli
3. ve validačních metodách je kontrolována duplicita emailu i telefonu, zbytek může být duplicitní
4. karta je vybrána pomocí sql (získám id karty s nejmenším číslem karty)
5. princip popsaný o bod výše, funguje jak pro první, tak pro následující karty.
6. \-
7. metoda v modelu využívající základní PHP funkci mail(). Šly by využít externí knihovny, jako například PHPMailer, či Nette\mail, ale sám bych asi odznovu kolo nevymýšlel