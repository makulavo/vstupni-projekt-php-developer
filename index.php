<?php

function autoload(string $class) : void
{
    if(preg_match("/Controller$/", $class)) {
        require ("Controller/".$class.".php");
    }elseif (preg_match("/Model$/", $class)) {
        require ("Model/".$class.".php");
    }
}

spl_autoload_register("autoload");

$router = new RouterController();
$router->process([$_SERVER["REQUEST_URI"]]);
$router->drawView();

